<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

Route::get('/posts', function () {
    return view('posts.index');
});


// REST API
Route::get('/fruits', [Controller::class, 'fruits']);