[source](https://tailwindcss.com/docs/guides/laravel)

```bash
composer create-project laravel/laravel todos-laravel-v1 "8.*" 
```

```html
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" >
```

## OR

# Tailwind
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
```

```js
// tailwind.config.js
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

```bash
npm run dev
```

https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css ref="stylesheet"